package pucrs.progoo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class AppTeste {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Acervo acervo = new Acervo();
		Cadastro cadastro = new Cadastro();
		Emprestimos listaEmp = new Emprestimos();
		
		System.out.println("Usuarios:"+Usuario.getTotalUsuarios());

		cadastro.cadastrar("1", "Fulano", "01929123132");
		cadastro.cadastrar("2", "Beltrano", "82816612122");
		cadastro.cadastrar("3", "Ciclano", "91277626212");
		cadastro.cadastrar("4", "Huguinho", "28156483929");
		cadastro.cadastrar("5", "Zezinho", "71626846671");
		cadastro.cadastrar("6", "Luizinho", "77691761727");
		cadastro.ordenaNome();
		
		System.out.println("Usuarios:"+Usuario.getTotalUsuarios());

		acervo.cadastrarLivro(new Livro(1, "O Senhor dos Anéis", "J. R. R. Tolkien", "Martins Fontes", 2000, 5));
		acervo.cadastrarLivro(new Livro(2, "A Origem das Espécies", "Charles Darwin", "Hemus", 2000, 2));
		acervo.cadastrarLivro(new Livro(3, "A Arte da Guerra", "Sun Tzu", "Pensamento", 1995, 3));
		// Exemplo do construtor sobrecarregado (assume total = 1)
		acervo.cadastrarLivro(new Livro(4, "Memórias Póstumas de Brás Cubas", "Machado de Assis", "Ática", 1998));
		acervo.cadastrarLivro(new Livro(5, "O Senhor dos Anéis", "J. R. R. Tolkien", "Martins Fontes", 1958, 1));
		acervo.ordenaTitulo();
//		acervo.ordenaCodigo();

		System.out.println("Usuários:");
		cadastro.listarTodos();

		System.out.println();
		System.out.println("Acervo:");
		acervo.listarTodos();
		
		// Exemplo: criando um empréstimo
		LocalDate dataLim = LocalDate.of(2016, 3, 23);
		LocalDate dataLim2= LocalDate.now().plusDays(7);
		//System.out.println(dataLim2);
		// Não esqueça de obter uma ref. para um Usuario e Livro
		Usuario usuario = cadastro.buscarCodigo("4");
		Livro livro = acervo.buscarPorCodigo(3);
		listaEmp.criar(usuario, livro, dataLim);
		
		System.out.println();
		listaEmp.listarTodos();
		
		System.out.println();
		Periodico per1 = new Periodico(10, "PUCRS Informação", "EDIPUCRS", 2015, 18, 4);
		System.out.println(per1);
		per1.emprestar();
		System.out.println(per1);
		
		ArrayList<Integer> lista = new ArrayList<>();
		lista.add(12);
		lista.add(7);
		lista.add(4);
		lista.add(8);
		lista.add(2);
		lista.add(9);
		Collections.sort(lista);
		System.out.println(lista);
		
		
		
		
		
	}

}
